package com.speakit.tts.foc.instance;
import com.speakit.tts.engine.PlayOption;
import com.speakit.tts.foc.utils.ZLIntegerRangeOption;
import com.speakit.tts.foc.utils.ZLSQLiteConfig;

import android.app.Application;

public class FOCApplication extends Application {
	private static FOCApplication ourApplication;
	/** 0-普通话 1-粤语 */
	public final ZLIntegerRangeOption TTSLanguageOption = new ZLIntegerRangeOption(
			"TTS", "languageType", 0, 1, 0);

	public final ZLIntegerRangeOption TTSLanguageToneOption = new ZLIntegerRangeOption(
			"TTS", "languageType", 3, 30, 10);

	/** 1-男0-女 */
	public final ZLIntegerRangeOption TTSSexOption = new ZLIntegerRangeOption(
			"TTS", "VoiceSex", 0, 1, 0);
	/** 慢速~快速 : 8~18 */
	public final ZLIntegerRangeOption TTSReadSpeedOption = new ZLIntegerRangeOption(
			"TTS", "ReadSpeed", PlayOption.MIN_READ_SPEED,
			PlayOption.MAX_READ_SPEED + 1, PlayOption.DEF_READ_SPEED);
	/** 0-开启后台播放 1-关闭后台播放 */
	public final ZLIntegerRangeOption TTSBackGroundPlayOption = new ZLIntegerRangeOption(
			"TTS", "BackGroundRead", 0, 1, 0);
	/** 设置播放停止的时间 范围：0~60 */
	public final ZLIntegerRangeOption TTSTimeToStopPlayOption = new ZLIntegerRangeOption(
			"TTS", "TimeToStop", 0, 120, 60);
	/** 设置seekBar字体设置 范围：20~50 */
	public final ZLIntegerRangeOption TTSSeekBarFondSizePlayOption = new ZLIntegerRangeOption(
			"TTS", "SeekBarFondSize", 20, 40, 28);

	public final ZLIntegerRangeOption SoundVolumePlayOption = new ZLIntegerRangeOption(
			"TTS", "SeekBarSoundVolume", 0, 10, 2);

	public static FOCApplication Instance() {
		return ourApplication;
	}

	public FOCApplication() {
		ourApplication = this;
	}
	@Override
	public void onCreate() {
		super.onCreate();
		new ZLSQLiteConfig(this);
	}
}