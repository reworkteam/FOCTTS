package com.speakit.tts.foc.instance;

import java.io.File;
import java.io.IOException;

import com.speakit.tts.engine.TTSPlayer;
import com.speakit.tts.foc.utils.L;
import com.speakit.tts.foc.utils.Utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.widget.Toast;

public class AndroidVoiceInstance {
	private final static String UPDATA_KEY = "isUpdate";
	private static TTSPlayer mPlayer;
	private final static String libPath = Utils.voiceLib
			+ "mandarin/xiaotian.dat";
	private static final int HIDE_SDWAITE = 0;
	private static final int SHOW_SDWAITE = 1;
	private static final int SHOW_ADDTTS = 2;
	private static final int ERROR_FOLDER = 3;
	private static Context myContext;
	private static boolean haveFinishInit = false;

	private static AndroidVoiceInstance instance = null;

	private static Handler processBarHandler = new Handler() {

		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {

			case HIDE_SDWAITE:
				mPlayer = TTSPlayer.getInstance(myContext);
				break;
			case SHOW_SDWAITE:
				Toast.makeText(myContext, "等待插入sd卡！", Toast.LENGTH_SHORT)
						.show();
				break;
			case SHOW_ADDTTS:
				Toast.makeText(myContext, "正在配置数据！", Toast.LENGTH_SHORT).show();
				break;
			case ERROR_FOLDER:
				Toast.makeText(myContext, "语音数据尚未准备好.", Toast.LENGTH_SHORT)
						.show();
				break;
			default:
				break;
			}
			super.handleMessage(msg);
		}
	};

	public static AndroidVoiceInstance Instance() {
		if (instance == null) {
			instance = new AndroidVoiceInstance();
		}
		return instance;
	}

	public void playVoiceString(final Context mContext, final String content) {
		new Thread(new Runnable() {
			public void run() {

				if (!TTSPlayer.getInstance(myContext).isPlaying()) {
					TTSPlayer.getInstance(myContext).getTTSAnnouncer()
							.playVoice(content);
				}
			}
		}, "mTTSPlayer").start();
	}

	public static void stopVoiceString() {
		new Thread(new Runnable() {
			public void run() {
				if (TTSPlayer.getInstance(myContext).isPlaying()) {
					mPlayer.getTTSAnnouncer().stopVoice();
				}
			}
		}, "mTTSPlayer").start();

	}

	private static int getVersionCode(Context context) {
		int versionCode = 1;
		PackageManager packageManager = context.getPackageManager();
		try {
			PackageInfo packageInfo = packageManager.getPackageInfo(
					context.getPackageName(), 0);
			versionCode = packageInfo.versionCode;
		} catch (NameNotFoundException e) {
			e.printStackTrace();
		}

		return versionCode;
	}

	public void initData(final Context mContext, String keyCode) {
		Utils.initContextCode(mContext, keyCode);
		myContext = mContext;
		final SharedPreferences sharedPreferences = mContext
				.getSharedPreferences("AndroidVoice", 0);
		final int versionNumber = sharedPreferences.getInt(UPDATA_KEY, 0);
		final int currentVersionCode = getVersionCode(mContext);
		// 初始化化数据库
		File urlFile = new File(Utils.voiceLib);
		if (!urlFile.exists()) {
			urlFile.mkdirs();
		}
		new Thread(new Runnable() {

			public void run() {

				while (!Environment.getExternalStorageState().equals(
						Environment.MEDIA_MOUNTED)) {
					processBarHandler.sendEmptyMessage(SHOW_SDWAITE);
					SystemClock.sleep(500);
				}

				if (versionNumber < currentVersionCode) {
					processBarHandler.sendEmptyMessage(SHOW_ADDTTS);
					Editor editor = null;
					editor = sharedPreferences.edit();
					editor.putInt(UPDATA_KEY, currentVersionCode);
					editor.commit();
					try {
						String zipFileString = Utils.voiceLib + "lib.zip";
						Utils.copyAssetToSdcard(mContext, "lib/focdata.zip",
								"lib.zip");
						CompresszZipFile.ReadZip(zipFileString, Utils.voiceLib);
						File file = new File(zipFileString);
						if (file.exists()) {
							file.delete();
						}
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
				processBarHandler.sendEmptyMessage(HIDE_SDWAITE);
				setFlag(true);
			}
		}).start();

		boolean flag = false;
		while (!flag) {
			flag = getFlag();
			L.l("=====count:" + flag);
			SystemClock.sleep(500);
		}

	}

	public static boolean getFlag() {
		return haveFinishInit;
	}

	public static void setFlag(boolean flag) {
		haveFinishInit = flag;
		L.l("=====ha:" + haveFinishInit);
	}

}
