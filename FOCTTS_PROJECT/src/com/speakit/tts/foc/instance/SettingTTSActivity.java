package com.speakit.tts.foc.instance;

import java.util.ArrayList;
import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import com.speakit.tts.engine.PlayOption;
import com.speakit.tts.engine.TTSPlayer;

public class SettingTTSActivity extends Activity {
	private ArrayList<String> ttsList = new ArrayList<String>();
	private TTSAdapter ttsAdapter;
	public static long timeNow = 0;
	public static long time = -1;
	protected static final int DIALOG_WATIING_PROCESS = 0;
	public static SettingTTSActivity showReadTTSInstance = null;
	public static int DEFAULT_LIGHT = 10;
	static boolean isBinded = false;
	private boolean isTTSPlayerPlaying = false;
	private ListView ttsListView;
	private Button selectManButton;
	private Button selectGirlButton;
	private Button returnBackButton;
	int foctts_setting_layout;
	int tts_set_list;
	int select_man;
	int select_girl;
	int backButton;
	int foctts_sex_selected;
	int foctts_sex_unselected;

	int title_tts;
	int btn_reset;
	int time_seekBar;

	int foctts_setting_listitem;

	// 反射得到
	public void refelectR() {
		Resources res = getResources();
		String tempPackageName = getPackageName();
		foctts_setting_layout = res.getIdentifier("foctts_setting_layout",
				"layout", tempPackageName);
		tts_set_list = res.getIdentifier("tts_set_list", "id", tempPackageName);
		select_man = res.getIdentifier("select_man", "id", tempPackageName);
		select_girl = res.getIdentifier("select_girl", "id", tempPackageName);
		backButton = res.getIdentifier("backButton", "id", tempPackageName);
		foctts_sex_selected = res.getIdentifier("foctts_sex_selected",
				"drawable", tempPackageName);
		foctts_sex_unselected = res.getIdentifier("foctts_sex_unselected",
				"drawable", tempPackageName);
		title_tts = res.getIdentifier("title_tts", "id", tempPackageName);
		btn_reset = res.getIdentifier("btn_reset", "id", tempPackageName);
		time_seekBar = res.getIdentifier("time_seekBar", "id", tempPackageName);
		foctts_setting_listitem = res.getIdentifier("foctts_setting_listitem",
				"layout", tempPackageName);

	}

	@Override
	public void onCreate(Bundle save) {
		super.onCreate(save);
		showReadTTSInstance = this;
		isTTSPlayerPlaying = TTSPlayer.getInstance(this).isPlaying();
		refelectR();
		setContentView(foctts_setting_layout);
		ttsListView = (ListView) findViewById(tts_set_list);
		playOption = new PlayOption();
		// 播放设置内容
		ttsList.add("音速");
		ttsList.add("音调");
		ttsList.add("睡眠");
		ttsAdapter = new TTSAdapter(SettingTTSActivity.this);
		ttsListView.setAdapter(ttsAdapter);
		setListViewHeightBasedOnChildren(ttsListView);
		selectManButton = (Button) findViewById(select_man);
		selectGirlButton = (Button) findViewById(select_girl);
		returnBackButton = (Button) findViewById(backButton);
		if (FOCApplication.Instance().TTSSexOption.getValue() == 1) {
			selectManButton.setBackgroundResource(foctts_sex_selected);
		} else {
			selectGirlButton.setBackgroundResource(foctts_sex_selected);
		}
		selectGirlButton.setOnClickListener(new SexSelectListener());
		selectManButton.setOnClickListener(new SexSelectListener());
		returnBackButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});

	}

	class SexSelectListener implements View.OnClickListener {

		@Override
		public void onClick(View v) {

			if (FOCApplication.Instance().TTSSexOption.getValue() == 1) {
				playOption.setSex(0);
				selectManButton.setBackgroundResource(foctts_sex_unselected);
				selectGirlButton.setBackgroundResource(foctts_sex_selected);
			} else {
				playOption.setSex(1);
				selectManButton.setBackgroundResource(foctts_sex_selected);
				selectGirlButton.setBackgroundResource(foctts_sex_unselected);
			}
		}
	}

	public final class KindViewHolder {
		public TextView kindName;
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();

	}

	public SettingTTSActivity() {
		// TODO Auto-generated constructor stub
	}

	public void onResume() {
		super.onResume();
	};

	@Override
	public void onBackPressed() {
		if (!isTTSPlayerPlaying)
			TTSPlayer.getInstance(this).getTTSAnnouncer().stopVoice();
		finish();
	}

	@Override
	public void onPause() {
		super.onPause();
		if (!isTTSPlayerPlaying) {
			playOption.setIsRun(false);
		}
	};

	PlayOption playOption;
	public boolean isFinish = false;

	public final class ViewHolderTTS {
		public TextView title;
		public Button btnReset;
		public SeekBar timeSeekBar;
	}

	public class TTSAdapter extends BaseAdapter {
		private LayoutInflater inflater;
		boolean isTTSDownLoad = false;
		boolean voiceLanguageEnable = false;
		float readSpeed = ((float) (PlayOption.MAX_READ_SPEED - PlayOption.MIN_READ_SPEED)) / 100;
		float toneSize = ((float) (PlayOption.MAX_TONE_SIZE - PlayOption.MIN_TONE_SIZE)) / 100;
		float timeStop = ((float) (PlayOption.MAX_TIME_STOP - PlayOption.MIN_TIME_STOP)) / 100;

		public TTSAdapter(Context context) {
			inflater = LayoutInflater.from(context);
		}

		@Override
		public int getCount() {
			return ttsList.size();
		}

		@Override
		public Object getItem(int position) {
			return ttsList.get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			ViewHolderTTS holder = null;
			if (convertView == null) {

				holder = new ViewHolderTTS();
				convertView = inflater.inflate(foctts_setting_listitem, null);
				holder.title = (TextView) convertView.findViewById(title_tts);
				holder.title.setTextSize(12);
				holder.btnReset = (Button) convertView.findViewById(btn_reset);
				holder.btnReset.setTextSize(12);

				holder.timeSeekBar = (SeekBar) convertView
						.findViewById(time_seekBar);

				convertView.setTag(holder);
			} else {
				holder = (ViewHolderTTS) convertView.getTag();
			}

			if (position == 0) {
				holder.title.setText(ttsList.get(position) + ":"
						+ (playOption.getReadSpeed()));
				holder.timeSeekBar.setVisibility(View.VISIBLE);
				holder.btnReset.setVisibility(View.VISIBLE);
				holder.btnReset.setText("重置");
				// holder.btnReset
				// .setBackgroundResource(foctts_button_reset);
				holder.btnReset.setVisibility(View.VISIBLE);
				holder.btnReset.setOnClickListener(new VoiceOnClickListenner(
						holder.title, holder.timeSeekBar));

				int voiceSpeed = playOption.getReadSpeed();
				holder.timeSeekBar.setProgress((int) (voiceSpeed / readSpeed));
				holder.timeSeekBar.setEnabled(true);
				holder.timeSeekBar
						.setOnSeekBarChangeListener(new ReadSpeedSeekBarTouchListener(
								holder));
			} else if (position == 1) {
				holder.title.setText(ttsList.get(position) + ":"
						+ (playOption.getReadTone()));
				holder.timeSeekBar.setVisibility(View.VISIBLE);
				holder.btnReset.setVisibility(View.VISIBLE);
				holder.btnReset.setText("重置");
				// holder.btnReset
				// .setBackgroundResource(foctts_button_reset);
				holder.btnReset.setVisibility(View.VISIBLE);
				holder.btnReset.setOnClickListener(new ToneOnClickListenner(
						holder.title, holder.timeSeekBar));

				int toneSize1 = playOption.getReadTone();
				holder.timeSeekBar.setProgress((int) (toneSize1 / toneSize));
				holder.timeSeekBar.setEnabled(true);
				holder.timeSeekBar
						.setOnSeekBarChangeListener(new ToneSizeSeekBarTouchListener(
								holder));
			} else if (position == 2) {
				holder.title.setText(ttsList.get(position) + ":"
						+ (playOption.getTimeToStop()) + "m");
				holder.timeSeekBar.setVisibility(View.VISIBLE);
				holder.btnReset.setVisibility(View.VISIBLE);
				holder.btnReset.setText("重置");
				// holder.btnReset
				// .setBackgroundResource(foctts_button_reset);
				holder.btnReset.setVisibility(View.VISIBLE);
				holder.btnReset.setOnClickListener(new TimeOnClickListenner(
						holder.title, holder.timeSeekBar));

				int time = playOption.getTimeToStop();
				holder.timeSeekBar.setProgress((int) (time / timeStop));
				holder.timeSeekBar.setEnabled(true);
				holder.timeSeekBar
						.setOnSeekBarChangeListener(new TimeSeekBarTouchListener(
								holder));

			}

			return convertView;
		}

		class ReadSpeedSeekBarTouchListener implements OnSeekBarChangeListener {
			private ViewHolderTTS holder;
			int voiceSpeed_c = 11;

			public ReadSpeedSeekBarTouchListener(ViewHolderTTS holder) {
				this.holder = holder;
				voiceSpeed_c = playOption.getReadSpeed();
			}

			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,
					boolean fromUser) {
				if (fromUser) {
					voiceSpeed_c = (int) (progress * readSpeed);
					holder.title.setText("音速:" + (voiceSpeed_c) + "");
				}
			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {

			}

			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
				playOption.setReadSpeed(voiceSpeed_c);
			}
		}

		/**
		 * 声音重置按钮
		 */
		class VoiceOnClickListenner implements OnClickListener {
			private TextView voiceReset;
			private SeekBar voiceResetSeekBar;

			public VoiceOnClickListenner(TextView voiceReset,
					SeekBar voiceResetSeekBar) {
				this.voiceReset = voiceReset;
				this.voiceResetSeekBar = voiceResetSeekBar;
			}

			@Override
			public void onClick(View v) {
				voiceReset.setText("音速:" + PlayOption.DEF_READ_SPEED + "");
				playOption.setReadSpeed(PlayOption.DEF_READ_SPEED);
				this.voiceResetSeekBar
						.setProgress(((int) (PlayOption.DEF_READ_SPEED / readSpeed)));
			}

		}

		class TimeSeekBarTouchListener implements OnSeekBarChangeListener {
			private ViewHolderTTS holder;
			int time_stop = 60;

			public TimeSeekBarTouchListener(ViewHolderTTS holder) {
				this.holder = holder;
				time_stop = playOption.getTimeToStop();
			}

			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,
					boolean fromUser) {
				// TODO Auto-generated method stub
				if (fromUser) {
					time_stop = (int) (progress * timeStop);
					holder.title.setText("" + (time_stop) + "分钟后睡眠");
				}
			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub
				FOCApplication.Instance().TTSTimeToStopPlayOption
						.setValue(time_stop);

			}
		}

		class ToneSizeSeekBarTouchListener implements OnSeekBarChangeListener {
			private ViewHolderTTS holder;
			int voiceSpeed_c = 10;

			public ToneSizeSeekBarTouchListener(ViewHolderTTS holder) {
				this.holder = holder;
				voiceSpeed_c = playOption.getReadTone();
			}

			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,
					boolean fromUser) {
				if (fromUser) {
					voiceSpeed_c = (int) (progress * toneSize);
					holder.title.setText("音调:" + (voiceSpeed_c) + "");
				}
			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {

			}

			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
				playOption.setReadTone(voiceSpeed_c);
			}
		}

		/**
		 * 音调设置按钮
		 */
		class TimeOnClickListenner implements OnClickListener {
			private TextView toneReset;
			private SeekBar toneResetSeekBar;

			public TimeOnClickListenner(TextView voiceReset,
					SeekBar voiceResetSeekBar) {
				this.toneReset = voiceReset;
				this.toneResetSeekBar = voiceResetSeekBar;
			}

			@Override
			public void onClick(View v) {
				toneReset.setText("" + PlayOption.DEF_TIME_STOP + "分钟后睡眠");
				playOption.setTimeToStop(PlayOption.DEF_TIME_STOP);
				this.toneResetSeekBar
						.setProgress(((int) (PlayOption.DEF_TIME_STOP / timeStop)));
			}

		}

		/**
		 * 音调设置按钮
		 */
		class ToneOnClickListenner implements OnClickListener {
			private TextView toneReset;
			private SeekBar toneResetSeekBar;

			public ToneOnClickListenner(TextView voiceReset,
					SeekBar voiceResetSeekBar) {
				this.toneReset = voiceReset;
				this.toneResetSeekBar = voiceResetSeekBar;
			}

			@Override
			public void onClick(View v) {
				toneReset.setText("音调:" + PlayOption.DEF_TONE_SIZE + "");
				playOption.setReadTone(PlayOption.DEF_TONE_SIZE);
				this.toneResetSeekBar
						.setProgress(((int) (PlayOption.DEF_TONE_SIZE / toneSize)));
			}

		}

	}

	/**
	 * 重新设定listview的高度
	 * 
	 * @param listView
	 */
	public static void setListViewHeightBasedOnChildren(ListView listView) {
		ListAdapter listadapter = listView.getAdapter();
		if (listadapter == null) {
			return;
		}

		// 整个listview的高度
		int listViewHeight = 0;
		for (int index = 0; index < listadapter.getCount(); index++) {
			View listItem = listadapter.getView(index, null, listView);
			listItem.measure(0, 0);
			listViewHeight += listItem.getMeasuredHeight();
		}

		// 重新设置listview的高度
		ViewGroup.LayoutParams params = listView.getLayoutParams();
		params.height = listViewHeight
				+ (listView.getDividerHeight() * (listadapter.getCount() - 1));
		listView.setLayoutParams(params);
	}

}
