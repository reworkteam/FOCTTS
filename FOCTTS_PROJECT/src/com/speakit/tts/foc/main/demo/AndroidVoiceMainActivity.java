package com.speakit.tts.foc.main.demo;

import com.androidvoice.main.demo.R;
import com.speakit.tts.foc.instance.AndroidVoiceInstance;
import com.speakit.tts.foc.instance.SettingTTSActivity;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class AndroidVoiceMainActivity extends Activity {
	Button showSettingTTSButton;
	Button playStringButton;
	Button stopStringButton;
	EditText inputEditText;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		AndroidVoiceInstance.Instance().initData(this, "59DCC4");
		setContentView(R.layout.foctts_main_layout);
		showSettingTTSButton = (Button) findViewById(R.id.settingtts);
		playStringButton = (Button) findViewById(R.id.play);
		stopStringButton = (Button) findViewById(R.id.stop);
		inputEditText = (EditText) findViewById(R.id.helloword);
		showSettingTTSButton.setOnClickListener(new MyClickListener());
		playStringButton.setOnClickListener(new MyClickListener());
		stopStringButton.setOnClickListener(new MyClickListener());

	}

	class MyClickListener implements View.OnClickListener {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if (v.getId() == R.id.settingtts) {
				AndroidVoiceInstance.stopVoiceString();
				Intent intent = new Intent(AndroidVoiceMainActivity.this,
						SettingTTSActivity.class);
				startActivity(intent);

			} else if (v.getId() == R.id.stop) {
				AndroidVoiceInstance.stopVoiceString();
			} else {
				String inputString = inputEditText.getText() + "";
				AndroidVoiceInstance.Instance().playVoiceString(
						AndroidVoiceMainActivity.this, inputString);
				
			}
		}

	}

	@Override
	public void onResume() {
		super.onResume();

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_android_voice_main, menu);
		return true;
	}

}
