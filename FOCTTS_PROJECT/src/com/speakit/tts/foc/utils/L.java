package com.speakit.tts.foc.utils;
import java.util.Hashtable;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.view.Window;
import android.view.WindowManager;

public class L {
	public static boolean logflag = false;
	public static String openAutoGetCover = "false";
	private static SharedPreferences sharedPreferences;
	private static Editor editor = null;
	private static boolean epubFlag = false;

	public static void setEpubFlag(boolean tag) {
		epubFlag = tag;
	}

	public static boolean getEpubFlag() {
		return epubFlag;
	}

	public static boolean hasTile = true;
	public static boolean fbTile = true;

	public static void showTitle(Activity context) {
		if (hasTile) {
			context.requestWindowFeature(Window.FEATURE_NO_TITLE);
			context.getWindow().setFlags(
					WindowManager.LayoutParams.FLAG_FULLSCREEN,
					WindowManager.LayoutParams.FLAG_FULLSCREEN);
		}
	}

	public static void fbreaderTitle(Activity context) {
		if (fbTile) {
			context.requestWindowFeature(Window.FEATURE_NO_TITLE);
			context.getWindow().setFlags(
					WindowManager.LayoutParams.FLAG_FULLSCREEN,
					WindowManager.LayoutParams.FLAG_FULLSCREEN);
		}
	}

	// ?��?记�???????次�???��???
	public static int SHOW_TIMES = 100;
	public static int JIFEN_MAX = -1;

	public static void setYoumiJifen(Context context, int value) {
		SharedPreferences sharedPreferences = context.getSharedPreferences(
				"ads", 0);
		Editor editor = sharedPreferences.edit();
		editor.putInt("jifen", value);
		editor.commit();
	}

	public static void setPlayCount(Context context, int value) {
		SharedPreferences sharedPreferences = context.getSharedPreferences(
				"ads", 0);
		Editor editor = sharedPreferences.edit();
		editor.putInt("count", value);
		editor.commit();
	}

	public static boolean getShowAds(Context context) {
		boolean flag = false;
		SharedPreferences sharedPreferences = context.getSharedPreferences(
				"ads", 0);
		int count = sharedPreferences.getInt("count", 0);
		if (count >= SHOW_TIMES) {
			flag = true;
		}
		return flag;
	}

	public static int getCount(Context context) {
		SharedPreferences sharedPreferences = context.getSharedPreferences(
				"ads", 0);
		int count = sharedPreferences.getInt("count", 0);
		return count;
	}

	public static int getJifen(Context context) {
		SharedPreferences sharedPreferences = context.getSharedPreferences(
				"ads", 0);
		int jifen = sharedPreferences.getInt("jifen", 0);
		return jifen;
	}

	public static boolean getJifenCountAccess(Context context) {
		SharedPreferences sharedPreferences = context.getSharedPreferences(
				"ads", 0);
		int jifen = sharedPreferences.getInt("jifen", 0);
		int count = sharedPreferences.getInt("count", 0);

		if (count <= SHOW_TIMES) {
			return true;
		}
		if (jifen > JIFEN_MAX) {
			return true;
		} else {
			return false;
		}
	}

	/*
	 * 
	 * �??????��???ist
	 */

	
	public static int getBackgroundId(Context context) {
		SharedPreferences sharedPreferences = context.getSharedPreferences(
				"ads", 0);
		int jifen = sharedPreferences.getInt("backid", -1);
		return jifen;
	}

	public static void setBackgroundId(Context context, int rid) {
		SharedPreferences sharedPreferences = context.getSharedPreferences(
				"ads", 0);
		Editor editor = sharedPreferences.edit();
		editor.putInt("backid", rid);
		editor.commit();
	}

	/**
	 * ?��???????�????��
	 */
	public static void getCoverConfig(Context context) {
		sharedPreferences = context.getSharedPreferences("DocinReader", 0);
		L.openAutoGetCover = sharedPreferences.getString("cover", "");
		if ("".equals(sharedPreferences.getString("cover", ""))) {
			editor = sharedPreferences.edit();
			editor.putString("cover", "false");
			editor.commit();
		}
	}

	public static void setCoverConfig(Context context, boolean auto) {
		sharedPreferences = context.getSharedPreferences("DocinReader", 0);
		editor = sharedPreferences.edit();
		L.l("-----------config --- auto: " + auto);
		if (auto) {
			editor.putString("cover", "true");
		} else {
			editor.putString("cover", "false");
		}
		editor.commit();
	}

	public static void l(String info) {
		if (!logflag) {
			return;
		}
		final StackTraceElement[] stack = new Throwable().getStackTrace();
		final int i = 1;
		// for(int id = 0; id < stack.length; id++){
		final StackTraceElement ste = stack[i];
		// android.util.Log.e(SharpParam.LOG_TAG,
		// String.format("[%s][%s]%s[%s]", ste.getFileName(),
		// ste.getMethodName(), ste.getLineNumber(), info));
		android.util.Log.e(
				"ppdushu",
				String.format("[%s][%s]%s[%s]", ste.getFileName(),
						ste.getMethodName(), ste.getLineNumber(), info));
		// }
	}

	static Hashtable<String, Long> startTimeHashtable = new Hashtable<String, Long>();

	/**
	 * ???�????ame
	 * 
	 * @param name
	 */
	public static void startTime(String name) {
		if (!logflag) {
			return;
		}
		if (!startTimeHashtable.containsKey(name)) {
			startTimeHashtable.put(name, System.currentTimeMillis());
		}
	}

	/**
	 * @param name
	 *            ??????�??
	 */
	public static void endTime(String name) {
		if (!logflag) {
			return;
		}
		if (startTimeHashtable.containsKey(name)) {
			float wasteTime = ((float) (System.currentTimeMillis() - startTimeHashtable
					.get(name)) / 1000);
			android.util.Log.e("======" + name + "===wasteTime:", wasteTime
					+ " (s)");
			startTimeHashtable.remove(name);
		} else {
			android.util.Log.e("======" + name + "===error", "========");
		}
	}

}