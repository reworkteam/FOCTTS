package com.speakit.tts.foc.utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Environment;

public class Utils {

	public static String voiceLib = Environment.getExternalStorageDirectory()
			+ "/androidvoice/lib/";
	public static Context mContext = null;
	public static String mCode = "";
	private static final char HEX_DIGITS[] = { '0', '1', '2', '3', '4', '5',
		'6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };

	public static byte[] join(byte[] arr1, byte[] arr2) {
		if (arr1 != null && arr2 != null) {
			byte[] result = new byte[arr1.length + arr2.length];

			System.arraycopy(arr1, 0, result, 0, arr1.length);
			System.arraycopy(arr2, 0, result, arr1.length, arr2.length);

			arr1 = null;
			arr2 = null;
			return result;
		} else if (arr1 != null && arr2 == null) {
			return arr1;
		} else if (arr1 == null && arr2 != null) {
			return arr2;
		} else {
			return null;
		}
	}

	public static void copyAssetToSdcard(Context context, String fromName,
			String toName) throws IOException {

		File mWorkingPath = new File(voiceLib);
		if (!mWorkingPath.exists()) {
			if (!mWorkingPath.mkdirs()) {
			}
		}
		File outFile = new File(voiceLib + toName);
		if (outFile.exists())
			outFile.delete();
		OutputStream out = new FileOutputStream(outFile);

		InputStream in = null;
		in = context.getAssets().open(fromName);
		byte[] buf = new byte[1024];
		int len;
		while ((len = in.read(buf)) > 0) {
			out.write(buf, 0, len);
		}
		in.close();
		out.close();
	}

	public static String getSDPath() {
		File sdDir = null;
		boolean sdCardExist = Environment.getExternalStorageState().equals(
				Environment.MEDIA_MOUNTED); // 判断sd卡是否存在
		if (sdCardExist) {
			sdDir = Environment.getExternalStorageDirectory();// 获取跟目录
			return sdDir.toString();
		} else {
			return null;
		}
	}

	public static void initContextCode(Context context, String code) {
		mContext = context;
		mCode = code;
	}

	public static boolean checkKeyCode(Context context, String code) {
		String packageNmae = "";
		try {
			PackageManager packageManager = context.getPackageManager();
			PackageInfo packageInfo = packageManager.getPackageInfo(
					context.getPackageName(), 0);
			packageNmae = packageInfo.packageName;
		} catch (NameNotFoundException e) {
			e.printStackTrace();
		}
		return checkDataaSuccess(packageNmae, code);

	}

	

	public static String md5sum(String s) {
		try {
			MessageDigest digest = java.security.MessageDigest
					.getInstance("MD5");
			digest.update(getStringCodeBytes(s));
			byte messageDigest[] = digest.digest();
			return toHexString(messageDigest).substring(0, 6);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return "";
	}

	private static byte[] getStringCodeBytes(String s) {

		return (s).getBytes();
	}

	private static String toHexString(byte[] b) {
		StringBuilder sb = new StringBuilder(b.length * 2);
		for (int i = 0; i < b.length; i++) {
			sb.append(HEX_DIGITS[(b[i] & 0xf0) >>> 4]);
			sb.append(HEX_DIGITS[b[i] & 0x0f]);
		}
		return sb.toString();
	}

	public native static boolean checkDataaSuccess(String p, String key);

	static {
		System.loadLibrary("FOCTTSDATA");
	}

}
