package com.speakit.tts.engine;

import javax.security.auth.PrivateCredentialPermission;

import android.content.Context;

import com.speakit.tts.foc.utils.L;

public class TTSPlayer {
	public final static String WELCOME_TEXT = "感谢使用免费离线中文语音引擎！";
	private static TTSPlayer instance = null;
	private TTSAnnouncer ttsAnnouncer;
	private static boolean readEnd = false;
	private static boolean isPlaying = false;
	private Context mContext;

	public static TTSPlayer getInstance(Context context) {
		if (instance == null)
			instance = new TTSPlayer(context);
		return instance;
	}

	public boolean isPlaying() {
		L.l("====isPlaying====" + isPlaying);
		return isPlaying;
	}

	public void setPlaying(boolean flag) {
		isPlaying = flag;
	}

	public boolean isReadEnd() {
		return readEnd;
	}

	public void setReadEnd(boolean flag) {
		readEnd = flag;
	}

	public TTSAnnouncer getTTSAnnouncer() {
		return ttsAnnouncer;
	}

	private TTSPlayer(Context context) {
		mContext=context;
		ttsAnnouncer = new TTSAnnouncer(context);

		ttsAnnouncer.setOnReadListener(new TTSAnnouncer.OnReadListener() {
			@Override
			public void onUserStop() {
				L.l("======onUserStop====");
				setPlaying(false);
				setReadEnd(true);
			}

			@Override
			public void onReadEnd() {
				setPlaying(false);
				setReadEnd(true);
			}

			@Override
			public void onInitEnginError(int ErrorCode) {
				setPlaying(false);
				setReadEnd(true);
			}

			@Override
			public void onSubStringReadEnd() {
				setPlaying(false);
				setReadEnd(true);
			}

			@Override
			public void onSettingChange() {
				TTSPlayer.getInstance(mContext).ttsAnnouncer
						.playVoice(TTSPlayer.WELCOME_TEXT);
				
			}

			@Override
			public void onPlaying() {
				L.l("========tts====playing==");
				setPlaying(true);
				setReadEnd(false);
			}
		});
	}
}
