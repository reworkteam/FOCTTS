﻿package com.speakit.tts.engine;

//input text callback type
public interface ITTSInputTextProc {
	/**
	 * 文本输入回调抽象接口
	 * 
	 * @param pText
	 *            待合成的文本内容
	 * @param piSize
	 *            文本内容长度
	 * @return 0表示成功,非0表示失败
	 */
	abstract public int callBackProc(ByteBuffer pText, LongInt piSize);
}
