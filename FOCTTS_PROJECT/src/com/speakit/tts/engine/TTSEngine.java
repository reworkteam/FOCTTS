﻿package com.speakit.tts.engine;

import android.content.res.AssetManager;

public class TTSEngine {

	public static final int hwTTS_ERR_NONE = 0;

	protected static final native int hwTTSInit(AssetManager ass,String szCNLib, String szENLib,
			String szDMLib, LongInt pdwHandle);

	protected static final native int hwTTSEnd(long dwHandle);

	public static final int PARAM_READ_TONE = 1;
	public static final int PARAM_READ_SPEED = 2;
	public static final int PARAM_READ_VOICE = 3;
	public static final int PARAM_CUT_BY_BACKSPACE = 4;

	protected static final native int hwTTSSetParam(long dwHandle, int nParam,
			long iValue);

	protected static final native int hwTTSSynthStop(long dwHandle);

	protected static final native int hwTTSSynthesize(long dwHandle);

	protected static final native int hwTTSSetInputTextCB(long dwHandle,
			ITTSInputTextProc iCallBack);

	protected static final native int hwTTSSetOutputVoiceCB(long dwHandle,
			ITTSOutputVoiceProc iCallBack);

	protected static final native float hwTTSGetLibVersion();

	protected static native void sttsPlay(int ttsHandle, byte[] text);

	private static native void initIDs();

	static {
		System.loadLibrary("FOCTTS");
		initIDs();
	}
}
