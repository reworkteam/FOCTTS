package com.speakit.tts.engine;

import com.speakit.tts.engine.TTSAnnouncer;
import com.speakit.tts.foc.instance.FOCApplication;

public class PlayOption {
	private int languageType = 0;
	private int sex = 1;
	private int readSpeed = 11;
	private int readTone = 10;
	private int bgPlay = 0;
	private int timeToStop = 60;
	private int backgroudSoundSize = 2;
	public static final int MAX_READ_SPEED = 30;
	public static final int MIN_READ_SPEED = 0;
	public static final int DEF_READ_SPEED = 11;
	
	public static final int MAX_TONE_SIZE = 30;
	public static final int DEF_TONE_SIZE = 10;
	public static final int MIN_TONE_SIZE = 0;
	
	public static final int MAX_TIME_STOP = 120;
	public static final int DEF_TIME_STOP = 60;
	public static final int MIN_TIME_STOP = 0;

	public PlayOption() {
		languageType = FOCApplication.Instance().TTSLanguageOption.getValue();
		sex = FOCApplication.Instance().TTSSexOption.getValue();
		readSpeed = FOCApplication.Instance().TTSReadSpeedOption.getValue();
		bgPlay = FOCApplication.Instance().TTSBackGroundPlayOption.getValue();
		timeToStop = FOCApplication.Instance().TTSTimeToStopPlayOption
				.getValue();
		readTone = FOCApplication.Instance().TTSLanguageToneOption.getValue();

		backgroudSoundSize = FOCApplication.Instance().SoundVolumePlayOption
				.getValue();
	}

	public int getSoundVolume() {
		return timeToStop;
	}

	public void setSoundVolume(int volume) {
		if (this.backgroudSoundSize == volume)
			return;
		this.backgroudSoundSize = volume;
		FOCApplication.Instance().SoundVolumePlayOption.setValue(volume);
	}

	public int getTimeToStop() {
		return timeToStop;
	}

	public void setTimeToStop(int timeToStop) {
		if (this.timeToStop == timeToStop)
			return;
		this.timeToStop = timeToStop;
		FOCApplication.Instance().TTSTimeToStopPlayOption.setValue(timeToStop);
	}

	/** 0-普通话 1-粤语 */
	public int getLanguageType() {
		return languageType;
	}

	/** 0-普通话 1-粤语 */
	public void setLanguageType(int languageType) {
		if (this.languageType == languageType)
			return;
		this.languageType = languageType;
		FOCApplication.Instance().TTSLanguageOption.setValue(languageType);
		this.settingChange();
	}

	/** 0-男 1-女 */
	public int getSex() {
		return sex;
	}

	/** 0-男 1-女 */
	public void setSex(int sex) {
		if (this.sex == sex)
			return;
		this.sex = sex;
		FOCApplication.Instance().TTSSexOption.setValue(sex);
		this.settingChange();
	}

	/** 8-慢速 ~ 30-快速 */
	public int getReadSpeed() {
		return FOCApplication.Instance().TTSReadSpeedOption.getValue();
	}

	/** 8-慢速 ~ 30-快速 */
	public void setReadSpeed(int readSpeed) {
		if (this.readSpeed == readSpeed)
			return;
		this.readSpeed = readSpeed;
		FOCApplication.Instance().TTSReadSpeedOption.setValue(readSpeed);
		this.settingChange();
	}

	// 设置音调
	public void setReadTone(int toneSize) {
		if (this.readTone == toneSize)
			return;
		this.readTone = toneSize;
		FOCApplication.Instance().TTSLanguageToneOption.setValue(readTone);
		this.settingChange();
	}

	public int getReadTone() {
		return this.readTone;
	}

	/** 0-开启后台播放 1-关闭后台播放 */
	public int getBgPlay() {
		return bgPlay;
	}

	/** 0-开启后台播放 1-关闭后台播放 */
	public void setBgPlay(int bgPlay) {
		if (this.bgPlay == bgPlay)
			return;
		this.bgPlay = bgPlay;
		FOCApplication.Instance().TTSBackGroundPlayOption.setValue(bgPlay);
	}

	int clickTime = 0;
	int LAG_TIME = 1000; // MS

	public boolean isRun = true;

	public synchronized boolean getIsRun() {
		return isRun;
	}

	public synchronized void setIsRun(boolean isRun) {
		this.isRun = isRun;
	}

	private void settingChange() {
		clickTime = 0;
		isRun = true;
		if (sendMsgThread == null) {
			sendMsgThread = new Thread(new Runnable() {
				@Override
				public void run() {

					while (getIsRun()) {
						try {
							Thread.sleep(50);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}

						clickTime++;
						if (clickTime == LAG_TIME / 25) {
							if (TTSAnnouncer.instance != null) {
								TTSAnnouncer.instance.setChange();
								// MM.sysout("发送!");
							}
						}

						if (clickTime == (LAG_TIME / 10)) {
							clickTime = (LAG_TIME / 10) - 1;
						}
					}
				}
			});
			sendMsgThread.start();
		}

	}

	Thread sendMsgThread = null;
}