﻿package com.speakit.tts.engine;

public interface ITTSOutputVoiceProc {
	/**
	 * 音频输出回调函数
	 * 
	 * @param iOutputFormat
	 *            语音数据音频格式
	 * @param pVoiceData
	 *            语音数据
	 * @param iVoiceSize
	 *            语音数据长度 0代表合成结束, -1 代表不发音
	 * @return 0成功,非0表示失败
	 */
	abstract public int callBackProc(long iOutputFormat, ByteBuffer pVoiceData,
			long iVoiceSize);

}
