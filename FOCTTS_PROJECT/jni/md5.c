#include <stdio.h>   
#include <string.h>   
#include <jni.h>
#include<android/log.h>

jboolean Java_com_speakit_tts_foc_utils_Utils_checkDataaSuccess(JNIEnv* env,
		jclass clazz, jstring p, jstring key) {
	const char *package = (*env)->GetStringUTFChars(env, p, NULL);
	const char *keycode = (*env)->GetStringUTFChars(env, key, NULL);
	const char *addString = "nil*";
	char *endPackage = NULL;
	endPackage = (char*) malloc(
			(strlen(package) + strlen(addString)) * (sizeof(char)));
	endPackage = strcat(package, addString);
	jclass cls = (*env)->FindClass(env, "com/speakit/tts/foc/utils/Utils");
	if (cls != 0) {
		// 获取方法ID, 通过方法名和签名, 调用静态方法
		jmethodID mid = (*env)->GetStaticMethodID(env, cls, "md5sum",
				"(Ljava/lang/String;)Ljava/lang/String;");
		if (mid != 0) {
			jstring arg = (*env)->NewStringUTF(env, endPackage);
			jstring result = (jstring)(*env)->CallStaticObjectMethod(env, cls,
					mid, arg);
			const char* str = (*env)->GetStringUTFChars(env, result, 0);
			(*env)->ReleaseStringUTFChars(env, result, 0);
			(*env)->ReleaseStringUTFChars(env, arg, 0);
			if (strcmp(str, keycode) == 0) {
				__android_log_print(ANDROID_LOG_INFO, "==FOCTTS:",
						"key success!");
				return JNI_TRUE;
			} else {
				__android_log_print(ANDROID_LOG_INFO, "==FOCTTS:",
						"key failed!");
				return JNI_FALSE;
			}

		} else {
			return JNI_FALSE;
		}
	} else {
		return JNI_FALSE;
	}

}

